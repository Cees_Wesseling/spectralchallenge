# Task description as received

Python gRPC-based microservice:

Create a gRPC server that serves the time based electricity consumption data found in meterusage.csv

Using whatever tool you prefer, create a HTTP server that will request the data from the gRPC server and deliver the consumption data as JSON.

Create a single page HTML document that requests the JSON from the HTTP server and displays it.

Please supply a link to your work in a public repository and a short README with a description of what you did.

# Analysis

* gRPC Server
* HTTP Server is a gRPC Client to the gRPC Server
* As worded the HTTP Server serves a single page HTML document that embeds a request for the timeseries data as JSON. Which implies JavaScript in the document doing the HTTP GET.

Minimal setup are 2 processes gRPCServer and HTTPServer.

# Iteration 1

First set up the structure passing "Hello World" as a string payload. Deal with the actual timeseries data when that is running.

Per debian 10 / python 3
```
sudo apt-get install python3-grpc-tools
sudo pip3 install grpcio
sudo pip3 install Flask
```

Running first setup:
```
cd src
make test # will generate grpc code
bash runServers.bash
```

Committed as first setup

# Data Iteration

Parse data minimal error checking using numpy.
```
sudo pip3 install numpy
```

Need test cases let's create a timeseries service: returning certain data depending on a name

* "empty"           no values
* "2020jan1"        new year 2020 and metervalue 1.0
* "meterusage"     ../data/meterusage.csv

Commited as final
