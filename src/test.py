import unittest

import gRPCServer
import HTTPServer
import Timeseries
import TimeseriesService
import datetime

class RPCResponsConstructionTest(unittest.TestCase):

    def test_empty(self):
        TimeseriesService.empty()

    def test_jan1_2020(self):
        TimeseriesService.jan1_2020()

    def test_get(self):
        TimeseriesService.get("meterusage")


class RPCCallTest(unittest.TestCase):

    def setUp(self):
        self.server = gRPCServer.create_gRPCServer()
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    def test_empty(self):
        timeseries = HTTPServer.get_timeseries_from_gRPCServer("empty")
        self.assertEqual(len(timeseries.samples), 0)

    def test_2020jan1(self):
        timeseries = HTTPServer.get_timeseries_from_gRPCServer("jan1_2020")
        self.assertEqual(len(timeseries.samples), 1)

        jan1_2020 = datetime.datetime(year=2020,month=1,day=1,
                                      hour=0,minute=0,second=0)

        dt = timeseries.samples[0].date.ToDatetime()
        self.assertEqual(dt, jan1_2020)

        self.assertEqual(timeseries.samples[0].value, 1.0)


class TimeseriesTest(unittest.TestCase):

    def test_read(self):
        t = Timeseries.Timeseries("../data/meterusage.csv")
        self.assertEqual(t.data.dtype,[('time', 'O'), ('meterusage', '<f8')])
        self.assertEqual(len(t.data), 2975)

        # 2019-01-01 00:15:00,55.09
        first = datetime.datetime(year=2019,month=1,day=1,
                                  hour=0,minute=15,second=0)
        self.assertEqual(t.data['time'][0], first)
        self.assertEqual(t.data['meterusage'][0], 55.09)

        # 2019-01-31 23:45:00,54.36
        last = datetime.datetime(year=2019,month=1,day=31,
                                  hour=23,minute=45,second=0)
        self.assertEqual(t.data['time'][-1], last)
        self.assertEqual(t.data['meterusage'][-1], 54.36)


if __name__ == "__main__":
    unittest.main()
