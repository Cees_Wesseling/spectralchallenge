"""
   HTTPServer embeds an RPCClient
"""
import flask
import grpc
import google.protobuf.json_format

import timeseriesService_pb2_grpc
import timeseriesService_pb2


def get_timeseries_from_gRPCServer(timeseriesId):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = timeseriesService_pb2_grpc.TimeseriesStub(channel)
        request = timeseriesService_pb2.TimeseriesDataRequest(timeseriesId=timeseriesId)
        response = stub.GetData(request)
    return response

app = flask.Flask("HTTPServer", template_folder=".")

@app.route('/')
def index():
    return flask.render_template("./index.html")

@app.route('/getTimeseries')
def getTimeseries():
    data = get_timeseries_from_gRPCServer("meterusage")
    dataJSON = google.protobuf.json_format.MessageToJson(data)
    response = app.response_class(
      response = dataJSON,
      status=200,
      mimetype='application/json'
    )
    return response
