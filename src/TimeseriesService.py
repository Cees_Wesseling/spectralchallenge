import datetime
import timeseriesService_pb2
import Timeseries

def _createPBResponse():
    """create protobuf defined TimeseriesDataResponse object"""
    return timeseriesService_pb2.TimeseriesDataResponse()

def _createPBSample(date, value):
    """create protobuf defined Sample object"""
    sample = timeseriesService_pb2.TimeseriesSample()
    sample.date.FromDatetime(date)
    sample.value = value
    return sample


def empty():
    return _createPBResponse()


def jan1_2020():
    response =  _createPBResponse()
    jan1_2020 = datetime.datetime(year=2020,month=1,day=1,
                                  hour=0,minute=0,second=0)
    response.samples.extend([_createPBSample(jan1_2020,1.0)])
    return response

def get(timeseriesId):
    """TODO fix dataset for now"""
    t = Timeseries.Timeseries("../data/meterusage.csv")
    samples = [ _createPBSample(t.data['time'][i],t.data['meterusage'][i]) for i in range(len(t.data))]
    response =  _createPBResponse()
    response.samples.extend(samples)
    return response
