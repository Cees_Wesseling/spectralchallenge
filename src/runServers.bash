#!/bin/bash
set -e

make test

python3 gRPCServer.py &
GRPC_SERVER_PID=$!

export FLASK_APP=HTTPServer.py
python3 -m flask run

kill $GRPC_SERVER_PID
