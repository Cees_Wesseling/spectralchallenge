import numpy as np
from datetime import datetime

class Timeseries(object):
    def __init__(self, filePath):
        # 1st 2 line example
        # time,meterusage
        # 2019-01-01 00:15:00,55.09
        str2datetime = lambda x:datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
        self.data = np.genfromtxt(filePath,
             dtype=None, names=True,delimiter=',', encoding='UTF-8',
             converters = {0:str2datetime})
