from concurrent import futures

import grpc

import timeseriesService_pb2_grpc

import TimeseriesService


class TimeseriesServicer(timeseriesService_pb2_grpc.TimeseriesServicer):

    def GetData(self, request, context):
        if request.timeseriesId == "empty":
            return TimeseriesService.empty()
        if request.timeseriesId == "jan1_2020":
            return TimeseriesService.jan1_2020()
        return TimeseriesService.get(request.timeseriesId)


def create_gRPCServer():
    """return a grpc.server with Timeseries service and port added"""
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    timeseriesService_pb2_grpc.add_TimeseriesServicer_to_server(TimeseriesServicer(), server)
    server.add_insecure_port('[::]:50051')

    return server

if __name__ == '__main__':
    server = create_gRPCServer()
    server.start()
    server.wait_for_termination()
